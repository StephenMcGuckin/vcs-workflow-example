


There are 4 steps to this workflow as follows
* Create a branch
* Make changes
* Submit a pull request
* Merge in the branch to `master`


## Create a branch
Do all your work on a separate branch. This way you can check in changes whenever and never have to worry
about stepping on someones toes.

Your branch should be something descriptive so that others can tell what your working on  
(e.g., `refactor-authentication`, `bugfix-broken-login-test`). 
   
To create a new branch
```bash
$ git checkout -b my-new-branch
```
To push your changes on the new branch
```bash
$ git push --set-upstream origin my-new-branch
```
Any subsequent pushes
    
```bash
$ git push
```
   
## Make changes
At this stage you should make all your commits in this branch.
Changes you make on a branch don't affect the master branch, so you're free to experiment and commit changes,
safe in the knowledge that your branch won't be merged until it's ready to be reviewed by someone you're 
collaborating with. Its often best to have commits that are modular and descriptive. This way your commits create a 
transparent history of your work that others can follow to understand what you've done and why.

## Submit a pull request
Once you have pushed and are ready for review open a Pull Request.
 




## Create
